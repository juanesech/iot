#define BLYNK_PRINT Serial

#include <ESP8266WiFi.h>
#include <BlynkSimpleEsp8266.h>

// You should get Auth Token in the Blynk App.
// Go to the Project Settings (nut iconl).
char auth[] = "cd4b12486e1b430583959a13a9764431";

// Your WiFi credentials.
// Set password to "" for open networks.
char ssid[] = "Republic_Guest";
char pass[] = "Tech-guest";

void setup()
{
  // Debug console
  Serial.begin(9600);

  Blynk.begin(auth, ssid, pass);
}

void loop()
{
  Blynk.run();
}
