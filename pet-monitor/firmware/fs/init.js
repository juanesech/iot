load('api_config.js');
load('api_mqtt.js');
load('api_timer.js');
load('api_gpio.js');
load('api_net.js');
load('api_sys.js');

let deviceName = Cfg.get('device.id');
let topic = '/devices/' + deviceName + '/events';
print('Topic: ', topic);

let isConnected = false;

let reflexPin = 13;
let buzzerPin = 15;

GPIO.set_mode(reflexPin, GPIO.MODE_INPUT);
GPIO.set_mode(buzzerPin, GPIO.MODE_OUTPUT);

let sensorStatus = function(pin){
  if (GPIO.read(pin) === 1) {
    GPIO.write(buzzerPin, 0);
    print('OFF');
    return "inactive";
  }else {
    GPIO.write(buzzerPin, 1);
    print('ON');
    return "active";
  }
};

let getInfo = function() {
  return JSON.stringify({
    state: sensorStatus(reflexPin)
  });
};

Timer.set(
  1000,
  true,
  function() {
    if (isConnected) {
      if (sensorStatus(reflexPin) === "active") {
        publishData();
      }
    }
  },
  null
);

Timer.set(
  1000,
  true,
  function() {
    print('Info:', getInfo());
  },
  null
);

MQTT.setEventHandler(function(conn, ev) {
  if (ev === MQTT.EV_CONNACK) {
    print('CONNECTED');
    isConnected = true;
    if (sensorStatus(reflexPin) === "active") {
      publishData();
    }
  }
}, null);

function publishData() {
  let ok = MQTT.pub(topic, getInfo());
  if (ok) {
    print('Published');
  } else {
    print('Error publishing');
  }
}

// Monitor network connectivity.
Net.setStatusEventHandler(function(ev, arg) {
  let evs = '???';
  if (ev === Net.STATUS_DISCONNECTED) {
    evs = 'DISCONNECTED';
  } else if (ev === Net.STATUS_CONNECTING) {
    evs = 'CONNECTING';
  } else if (ev === Net.STATUS_CONNECTED) {
    evs = 'CONNECTED';
  } else if (ev === Net.STATUS_GOT_IP) {
    evs = 'GOT_IP';
  }
  print('== Net event:', ev, evs);
}, null);
