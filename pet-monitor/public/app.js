document.addEventListener('DOMContentLoaded', function() {
    const db = firebase.database();

    // Create listeners
    const devicesRef = db.ref('/devices');

    // Register functions that update with last devices state
    devicesRef.on('value', function(snapshot) {
        let devices = snapshot.val();
        console.log(devices);
        fetchReportData();
    });

});

function secondsToHms(d) {
    d = Number(d);
    var h = Math.floor(d / 3600);
    var m = Math.floor(d % 3600 / 60);
    var s = Math.floor(d % 3600 % 60);

    var hDisplay = h > 0 ? h + (h == 1 ? " hour, " : " hours, ") : "";
    var mDisplay = m > 0 ? m + (m == 1 ? " minute, " : " minutes, ") : "";
    var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
    return hDisplay + mDisplay + sDisplay;
}

const reportDataUrl = '/getReportData';

function fetchReportData() {
    try{
        fetch(reportDataUrl)
            .then(function(response) {
                return response.json();
            })
            .then(function(myJson) {
                console.log(myJson);
                let dataTable = document.getElementById("data_table");
                let tableBody = document.createElement('tbody');
                tableBody.id="table_body";
                   for (let i in myJson) {
                    let dataPoints = myJson[i].data_points;
                    let date = myJson[i].data_hora;
                    let tr_data = document.createElement('tr');
                    let td_date = document.createElement('td');
                    td_date.appendChild(document.createTextNode(date.split(" 00:00:00")));
                    tr_data.appendChild(td_date);
                    let td_dataPoints = document.createElement('td');
                    let finalTime = secondsToHms(dataPoints);
                    td_dataPoints.appendChild(document.createTextNode(finalTime));
                    tr_data.appendChild(td_dataPoints);
                    tableBody.appendChild(tr_data);
                }
                dataTable.appendChild(tableBody);
                $("#table_body").replaceWith(tableBody);


            });
    }
    catch (e) {
        alert('Error getting report data');
    }
}