document.addEventListener('DOMContentLoaded', function() {
  const db = firebase.database();

  // Create listeners
  const devicesRef = db.ref('/devices');
  let jsonObj = fetch(reportDataUrl).then(response => response.json()).then(myJson => myJson);

  // Register functions that update with last devices state
  devicesRef.on('value', function(snapshot) {
    let devices = snapshot.val();
    console.log(devices);
    let jsonObjNew = fetch(reportDataUrl).then(response => response.json()).then(myJson => myJson);
    if (jsonObj !== jsonObjNew){
        fetchReportData();
    }
  });


  //let intervalID = window.setInterval(fetchReportData(json), 1000);

});

function makeTable(container, data) {
    let table = $("<table/>").addClass('CSSTableGenerator');
    $.each(data, function(rowIndex, r) {
        let row = $("<tr/>");
        $.each(r, function(colIndex, c) {
            row.append($("<t"+(rowIndex === 0 ?  "h" : "d")+"/>").text(c));
        });
        table.append(row);
    });
    return container.append(table);
}

function appendTableColumn(table, rowData) {
    let lastRow = $('<tr/>').appendTo(table.find('tbody:last'));
    $.each(rowData, function(colIndex, c) {
        lastRow.append($('<td/>').text(c));
    });

    return lastRow;
}

$(document).ready(function() {
    let table = makeTable(data);
    appendTableColumn(table, ["Calgary", "Ottawa", "Yellowknife"]);
});

const reportDataUrl = '/getReportData';

function fetchReportData() {
  try{
      fetch(reportDataUrl)
          .then(function(response) {
              return response.json();
          })
          .then(function(myJson) {
                  console.log(myJson);
                  let body = document.getElementsByTagName('body')[0];
                  let tbl = document.getElementById("data_table");
                  let tbdy = document.createElement('tbody');
                  for (let i in myJson) {
                      let dataPoints = myJson[i].data_points;
                      let date = myJson[i].data_hora;
                      let tr_data = document.createElement('tr');
                      let td_date = document.createElement('td');
                      td_date.appendChild(document.createTextNode(date));
                      tr_data.appendChild(td_date);
                      let td_dataPoints = document.createElement('td');
                      td_dataPoints.appendChild(document.createTextNode(dataPoints));
                      tr_data.appendChild(td_dataPoints);

                      tbdy.appendChild(tr_data);
                  }
                  tbl.appendChild(tbdy);
                  body.appenChild(tbl);

                  return myJson;
          });
  }
    catch (e) {
        alert('Error getting report data');
    }
}
