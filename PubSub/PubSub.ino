#include <ESP8266WiFi.h>
#include <PubSubClient.h>

const char* ssid = "IUSH";
const char* password = "";
const char* mqttServer = "m15.cloudmqtt.com";
const char* mqttUser = "sjcpbwvo";
const char* mqttPassword = "cMzgJabMu7hl";
const int mqttPort = 15375;

WiFiClient espClient;
PubSubClient client (espClient);

void setup() {
  Serial.begin(115200);
  WiFi.begin(ssid, password);
  while(WiFi.status() != WL_CONNECTED){
    delay(500);
    Serial.println("Connecting to wifi...");
  }
  Serial.println("Connected to wifi");
  client.setServer(mqttServer, mqttPort);
  client.setCallback(callback);

  while(!client.connected()){
    Serial.println("Connecting to MQTT Server...");
    if(client.connect("ESP8266Client", mqttUser, mqttPassword)){
      Serial.println("Connected to MQTT Server...");
    }else{
      Serial.println("Failed connection to MQTT Server...");
      Serial.println(client.state());
      delay(2000);
    }
  }
  client.publish("esp/prueba", "Hola desde el nodeMCU");
  client.subscribe("esp/prueba");
  
}

void callback(char* topic, byte* payload, unsigned int length){
  Serial.print("Message recived from topic ");
  Serial.print(topic);

  Serial.println("Message: ");
  for(int i = 0; i<length;i++){
    Serial.print((char)payload[i]); 
  }
  Serial.println();
  Serial.println("----------------------------------------------");
}

void loop() {
  client.loop();
  delay(5000);
  client.publish("esp/prueba", "This is a message from nodeMCU ESP8266");

}
