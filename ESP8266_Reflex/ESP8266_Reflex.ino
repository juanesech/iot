
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 
14CORE TEST CODE FOR 
E18-D80NK Infrared Distance Ranging Sensor 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
int const DIGITAL = 13;
int counter = 0;
void setup()  {
 
 Serial.begin(9600); //Start serial communication boud rate at 9600
 pinMode(DIGITAL,INPUT); //Pin 5 as signal input
 
}
void loop()  {
   delay(1000); 
   if(digitalRead(DIGITAL)==LOW)  {
    // If no signal print collision detected
     Serial.println("Collision Detected.");
     Serial.println(counter);
     while(counter < 30){
        delay(500);
        Serial.println(counter);
        counter++;
      }
     Serial.print("Counter: ");
     Serial.print(counter);
     while(counter > 0){
        delay(500);
        Serial.println(counter);
        counter = counter -1;
      }
   }
   else  {
     
   }
   int counter = 0;
}
